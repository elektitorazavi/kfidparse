kfidparse
=========

A simple script to parse camera .kfid files and extract video and
metadata from them. In order to just see the metadata, you can run:

    $ ./kfidparse.py 1.kfid -m

This will dump the value of all of the headers in the file, but the
video is not extracted. In order to also get video, you need to
provide an output file:

    $ ./kfidparse.py 1.kfid -o output.mkv

This uses ffmpeg to convert output to an .mkv file (no actual
re-encoding is performed). If you want to get raw video output you can
use the `-f` argument:

    $ ./kfidparse.py 1.kfid -o output.raw -f raw
