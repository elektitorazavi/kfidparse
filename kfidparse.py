#!/usr/bin/env python3

import struct
import argparse
import sys
import os
import subprocess
from tempfile import NamedTemporaryFile


def find_ffmpeg():
    result = subprocess.run(['which', 'ffmpeg'],
                            stdout=subprocess.PIPE)
    if result.returncode != 0:
        return None

    return result.stdout.strip().decode('ascii')


def main():
    parser = argparse.ArgumentParser(
        description='kfid file parser: extracts video and metadata '
        'kfid files.')

    parser.add_argument('input', help='Input file.')

    parser.add_argument(
        '--metadata-only', '-m', action='store_true', default=False,
        help='Only dump header values to output. No video is '
        'extracted.')

    parser.add_argument(
        '--output', '-o', metavar='FILENAME',
        help='Output file.')

    parser.add_argument(
        '--format', '-f', default='auto',
        choices=['auto', 'raw'],
        help='Output format. Possible values: auto, raw. "auto" uses '
        'ffmpeg to convert raw video to the desired format based on '
        'output filename extension.among these choices based on the file '
        'extension.')

    args = parser.parse_args()

    if not args.metadata_only and not args.output:
        print('Output file not specified.')
        exit(1)

    with open(args.input, 'rb') as f:
        data = f.read()

    video = b''

    offset = 0

    # SciBlockHeader
    #
    # NOTE: since we're using native byte-order & alignment here (no
    # <, >, etc at the beginning of the struct specifier string),
    # normal C padding rules are used, which means a four-byte padding
    # is expected after the second field. That is why the specifier
    # string seems to amount to 44 bytes while it's actually needs 48
    # bytes. Notice that, per documentation, no padding is added at
    # the beginning or the end, so we have an explicit padding field
    # at the end.
    count, fcount, dur, pts, dts, cid, padding = struct.unpack('QIQQQII', data[offset:offset+48])
    last_pts, last_dts = pts, dts

    print('SciBlockHeader')
    print('count={} fcount={} dur={} pts={} dts={} cid={}'.format(
        count, fcount, dur, pts, dts, cid
    ))
    print()
    offset += 48

    for i in range(fcount):
        # SciFrameHeader
        abs_fcount, size, pts, dts, dur = struct.unpack('QQQQQ', data[offset:offset+40])

        print('SciFrameHeader #{}'.format(i + 1))
        print('abs_fcount={} size={} pts={} dts={} dur={}'.format(
            abs_fcount, size, pts, dts, dur
        ))
        print('diff_pts={}ms diff_dts={}ms'.format((pts - last_pts) / 1e6, (dts - last_dts) / 1e6))
        print()

        last_pts, last_dts = pts, dts

        offset += 40
        video += data[offset:offset+size]
        offset += size

    assert offset == len(data)

    if not args.metadata_only:
        ffmpeg = find_ffmpeg()
        if args.format != 'raw' and not ffmpeg:
            print('WARNING: ffmpeg not found. Raw video data will be '
                  'written to output file.',
                  file=sys.stderr)
            args.format = 'raw'

        if args.format == 'raw':
            with open(args.output, 'wb') as f:
                f.write(video)
        else:
            with NamedTemporaryFile('wb', delete=False) as f:
                temp_file = f.name
                f.write(video)

            cmdline = [ffmpeg, '-i', temp_file, '-c:v', 'copy', args.output]
            print('Running ffmpeg: {}'.format(' '.join(cmdline)))
            result = subprocess.run(cmdline)
            if result.returncode != 0:
                print()
                print('WARNING: ffmpeg did not run successfully. Raw '
                      'video data will be written to output.')
                with open(args.output, 'wb') as f:
                    f.write(video)

            os.unlink(temp_file)


if __name__ == '__main__':
    main()
